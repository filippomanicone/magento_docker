#!/bin/bash

set -m

sleep 60

#Set up the index RAM quota (to be applied across the entire cluster).
curl -v -X POST http://127.0.0.1:8091/pools/default -d memoryQuota=512 -d indexMemoryQuota=256

#Set up services. (Note that %2C is the ASCII Hex mapping to the comma character.)
curl -v http://127.0.0.1:8091/node/controller/setupServices -d services=kv%2Cn1ql%2Cindex

#Set up your administrator-username and password.
curl -v http://127.0.0.1:8091/settings/web -d port=8091 -d username=couchbase -d password=password

#Set up a bucket
curl -u couchbase:password -v -X POST http://127.0.0.1:8091/pools/default/buckets -d 'threadsNumber=2&replicaIndex=0&replicaNumber=0&evictionPolicy=valueOnly&ramQuotaMB=128&bucketType=couchbase&name=default&authType=sasl&saslPassword='