<?php require '../config.php'; ?>
<?php loadClass('Helper')->authPage(); ?>
<?php

/*********************************************************************************
 *
 * I N I T I A L I Z A T I O N
 *
 *********************************************************************************/


/*************************************************************
 * Get availability
 *************************************************************/
$avail_php		= loadClass('Php')->isAvailable();
$avail_dns		= loadClass('Dns')->isAvailable();
$avail_httpd	= loadClass('Httpd')->isAvailable();
$avail_mysql	= loadClass('Mysql')->isAvailable();
$avail_oracle	= loadClass('Oracle')->isAvailable();
$avail_couchbase	= loadClass('MyCouchBase')->isAvailable();




/*************************************************************
 * Test Connectivity
 *************************************************************/

$connection = array();
$error	= null;

// ---- HTTPD (required) ----

$host	= $GLOBALS['HTTPD_HOST_NAME'];
$succ	= loadClass('Httpd')->canConnect($error, $host);
$connection['Httpd'][$host] = array(
	'error' => $error,
	'host' => $host,
	'succ' => $succ
);
$host	= loadClass('Httpd')->getIpAddress();
$succ	= loadClass('Httpd')->canConnect($error, $host);
$connection['Httpd'][$host] = array(
	'error' => $error,
	'host' => $host,
	'succ' => $succ
);
$host	= 'random.'.loadClass('Httpd')->getTldSuffix();
$succ	= loadClass('Httpd')->canConnect($error, $host);
$connection['Httpd'][$host] = array(
	'error' => $error,
	'host' => $host,
	'succ' => $succ
);
// ---- MYSQL ----
if ($avail_mysql) {
	$host	= $GLOBALS['MYSQL_HOST_NAME'];
	$succ	= loadClass('Mysql')->canConnect($error, $host, array('user' => 'root', 'pass' => loadClass('Helper')->getEnv('MYSQL_ROOT_PASSWORD')));
	$connection['MySQL'][$host] = array(
		'error' => $error,
		'host' => $host,
		'succ' => $succ
	);
	$host	= loadClass('Mysql')->getIpAddress();
	$succ	= loadClass('Mysql')->canConnect($error, $host, array('user' => 'root', 'pass' => loadClass('Helper')->getEnv('MYSQL_ROOT_PASSWORD')));
	$connection['MySQL'][$host] = array(
		'error' => $error,
		'host' => $host,
		'succ' => $succ
	);
	$host	= '127.0.0.1';
	$succ	= loadClass('Mysql')->canConnect($error, $host, array('user' => 'root', 'pass' => loadClass('Helper')->getEnv('MYSQL_ROOT_PASSWORD')));
	$connection['MySQL'][$host] = array(
		'error' => $error,
		'host' => $host,
		'succ' => $succ
	);
}
// ---- ORACKE ----
if ($avail_oracle) {
    $host	= $GLOBALS['ORACLE_HOST_NAME'];
    $succ	= loadClass('Oracle')->canConnect($error, $host, array('user' => 'system', 'pass' => 'oracle'));
    $connection['Oracle'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
    $host	= loadClass('Oracle')->getIpAddress();
    $succ	= loadClass('Oracle')->canConnect($error, $host, array('user' => 'system', 'pass' => 'oracle'));
    $connection['Oracle'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
    $host	= '127.0.0.1';
    $succ	= loadClass('Oracle')->canConnect($error, $host, array('user' => 'system', 'pass' => 'oracle'));
    $connection['Oracle'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
}

if ($avail_couchbase) {
    $host	= $GLOBALS['COUCHBASE_HOST_NAME'];
    $succ	= loadClass('MyCouchBase')->canConnect($error, $host,  array('user' => '', 'pass' => ''));
    $connection['CouchBase'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
    $host	= loadClass('MyCouchBase')->getIpAddress();
    $succ	= loadClass('MyCouchBase')->canConnect($error, $host,  array('user' => '', 'pass' => ''));
    $connection['CouchBase'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
    $host	= '127.0.0.1';
    $succ	= loadClass('MyCouchBase')->canConnect($error, $host,  array('user' => '', 'pass' => ''));
    $connection['CouchBase'][$host] = array(
        'error' => $error,
        'host' => $host,
        'succ' => $succ
    );
}




// ---- BIND (required)----
$host	= $GLOBALS['DNS_HOST_NAME'];
$succ	= loadClass('Dns')->canConnect($error, $host);
$connection['Bind'][$host] = array(
	'error' => $error,
	'host' => $host,
	'succ' => $succ
);
$host	= loadClass('Dns')->getIpAddress();
$succ	= loadClass('Dns')->canConnect($error, $host);
$connection['Bind'][$host] = array(
	'error' => $error,
	'host' => $host,
	'succ' => $succ
);


/*************************************************************
 * Test Health
 *************************************************************/
$HEALTH_TOTAL = 0;
$HEALTH_FAILS = 0;

foreach ($connection as $docker) {
	foreach ($docker as $conn) {
		if (!$conn['succ']) {
			$HEALTH_FAILS++;
		}
		$HEALTH_TOTAL++;
	}
}
$HEALTH_PERCENT = 100 - ceil(100 * $HEALTH_FAILS / $HEALTH_TOTAL);


/*********************************************************************************
 *
 * H T M L
 *
 *********************************************************************************/
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php echo loadClass('Html')->getHead(true); ?>
	</head>

	<body style="background: #1f1f1f;">
		<?php echo loadClass('Html')->getNavbar(); ?>


		<div class="container">


			<!-- ############################################################ -->
			<!-- Version/Health -->
			<!-- ############################################################ -->
			<div class="row">

				<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-hashtag"></i> Version</div>
						<div class="dash-box-body">
							<strong>Magento</strong> 0.4 <small>2018-12-04</small>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<img src="/assets/img/banner.png" style="width:100%;" />
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-bug" aria-hidden="true"></i> Health</div>
						<div class="dash-box-body">
							<div class="meter">
							  <span style="color:black; width: <?php echo $HEALTH_PERCENT; ?>%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $HEALTH_PERCENT; ?>%</span>
							</div>
						</div>
					</div>
				</div>

			</div><!-- /row -->


			<!-- ############################################################ -->
			<!-- DASH -->
			<!-- ############################################################ -->
			<div class="row">

				<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-cog" aria-hidden="true"></i> Base Stack</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('dns'); ?>
								</div>
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('php'); ?>
								</div>
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('httpd'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-database" aria-hidden="true"></i> SQL Stack</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('mysql'); ?>
								</div>
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('oracle'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
<div class="col-md-4 col-sm-4 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-file-o" aria-hidden="true"></i> NoSQL Stack</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-4" style="margin-bottom:15px;">
									<?php echo loadClass('Html')->getCirle('couchbase'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div><!-- /row -->


			<!-- ############################################################ -->
			<!-- Settings / Status -->
			<!-- ############################################################ -->

			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-info-circle" aria-hidden="true"></i> PHP Container Setup</div>
						<div class="dash-box-body">
							<table class="table table-striped table-hover table-bordered table-sm font-small">
								<p><small>You can also enter the php container and work from inside. The following is available inside the container:</small></p>
								<thead class="thead-inverse">
									<tr>
										<th colspan="2">Settings</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>uid</th>
										<td><?php echo loadClass('Php')->getUid(); ?></td>
									</tr>
									<tr>
										<th>gid</th>
										<td><?php echo loadClass('Php')->getGid(); ?></td>
									</tr>
									<tr>
										<th>vHost docroot dir</th>
										<td><?php echo loadClass('Helper')->getEnv('HTTPD_DOCROOT_DIR'); ?></td>
									</tr>
									<tr>
										<th>vHost config dir</th>
										<td><?php echo loadClass('Helper')->getEnv('HTTPD_TEMPLATE_DIR'); ?></td>
									</tr>
									<tr>
										<th>vHost TLD</th>
										<td>*.<?php echo loadClass('Httpd')->getTldSuffix(); ?></td>
									</tr>
									<tr>
										<th>DNS</th>
										<td><?php if ($avail_dns): ?>Enabled<?php else: ?><span class="text-danger">Offline</span><?php endif;?></td>
									</tr>
									<tr>
										<th>Postfix</th>
										<td><?php echo loadClass('Helper')->getEnv('ENABLE_MAIL') ? 'Enabled'  : '<span class="bg-danger">No</span> Disabled';?></td>
									</tr>
								</tbody>
							</table>

							<table class="table table-striped table-hover table-bordered table-sm font-small">
								<thead class="thead-inverse">
									<tr>
										<th colspan="2">Tools</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>composer</th>
										<td id="app_composer"></td>
									</tr>
									<tr>
										<th>drupal-console</th>
										<td id="app_drupalc"></td>
									</tr>
									<tr>
										<th>drush</th>
										<td id="app_drush"></td>
									</tr>
									<tr>
										<th>git</th>
										<td id="app_git"></td>
									</tr>
									<tr>
										<th>Laravel installer</th>
										<td id="app_laravel"></td>
									</tr>
									<tr>
										<th>mysqldump-secure</th>
										<td id="app_mds"></td>
									</tr>
									<tr>
										<th>node</th>
										<td id="app_node"></td>
									</tr>
									<tr>
										<th>npm</th>
										<td id="app_npm"></td>
									</tr>
									<tr>
										<th>Phalcon devtools</th>
										<td id="app_phalcon"></td>
									</tr>
									<tr>
										<th>Symfony installer</th>
										<td id="app_symfony"></td>
									</tr>
									<tr>
										<th>Wordpress cli</th>
										<td id="app_wpcli"></td>
									</tr>
								</tbody>
							</table>

						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offset-lg-4 offset-md-0 offset-sm-0 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-info-circle" aria-hidden="true"></i> PHP Container Status</div>
						<div class="dash-box-body">
							<p><small>The PHP Docker can connect to the following services via the specified hostnames and IP addresses.</small></p>
							<table class="table table-striped table-hover table-bordered table-sm font-small">
								<thead class="thead-inverse">
									<tr>
										<th>Service</th>
										<th>Hostname / IP</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($connection as $name => $docker): ?>
										<tr>
											<th rowspan="<?php echo count($docker);?>" class="align-middle"><?php echo $name; ?> connect</th>
											<?php $i=1; foreach ($docker as $conn): ?>

											<?php if ($conn['succ']): ?>
												<?php $text = '<span class="text-success dvlbox-ok"><i class="fa fa-check-square"></i></span> '.$conn['host']; ?>
											<?php else: ?>
												<?php $text = '<span class="text-danger dvlbox-err"><i class="fa fa-exclamation-triangle"></i></span> '.$conn['host'].'<br/>'.$conn['error']; ?>
											<?php endif; ?>

												<?php if ($i == 1): $i++;?>
													<td>
														<?php echo $text; ?>
													</td>
													</tr>
												<?php else: $i++;?>
													<tr>
														<td>
															<?php echo $text; ?>
														</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div><!-- /row -->


			<!-- ############################################################ -->
			<!-- TABLES -->
			<!-- ############################################################ -->
			<div class="row">

				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-share-alt" aria-hidden="true"></i> Networking</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="container">
									<table class="table table-striped table-hover table-bordered table-sm font-small">
										<thead class="thead-inverse">
											<tr>
												<th>Docker</th>
												<th>Hostname</th>
												<th>IP</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>php</th>
												<td><?php echo $GLOBALS['PHP_HOST_NAME']; ?></td>
												<td><?php echo loadClass('Php')->getIpAddress(); ?></td>
											</tr>
											<tr>
												<th>httpd</th>
												<td><?php echo $GLOBALS['HTTPD_HOST_NAME']; ?></td>
												<td><?php echo loadClass('Httpd')->getIpAddress(); ?></td>
											</tr>
											<?php if ($avail_mysql): ?>
												<tr>
													<th>mysql</th>
													<td><?php echo $GLOBALS['MYSQL_HOST_NAME']; ?></td>
													<td><?php echo loadClass('Mysql')->getIpAddress(); ?></td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_oracle): ?>
												<tr>
													<th>oracle</th>
													<td><?php echo $GLOBALS['ORACLE_HOST_NAME']; ?></td>
													<td><?php echo loadClass('Oracle')->getIpAddress(); ?></td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_dns): ?>
												<tr>
													<th>bind</th>
													<td><?php echo $GLOBALS['DNS_HOST_NAME']; ?></td>
													<td><?php echo loadClass('Dns')->getIpAddress(); ?></td>
												</tr>
												<tr>
											<?php endif; ?>
											<?php if ($avail_couchbase): ?>
												<th>couchbase</th>
													<td><?php echo $GLOBALS['COUCHBASE_HOST_NAME']; ?></td>
													<td><?php echo loadClass('MyCouchBase')->getIpAddress(); ?></td>
												</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offset-lg-4 offset-md-0 offset-sm-0 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-wrench" aria-hidden="true"></i> Ports</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="container">
									<table class="table table-striped table-hover table-bordered table-sm font-small">
										<thead class="thead-inverse">
											<tr>
												<th>Docker</th>
												<th>Host port</th>
												<th>Docker port</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>php</th>
												<td>-</td>
												<td>9000</td>
											</tr>
											<tr>
												<th>httpd</th>
												<td>
													<?php echo loadClass('Helper')->getEnv('LOCAL_LISTEN_ADDR').loadClass('Helper')->getEnv('HOST_PORT_HTTPD');?><br/>
													<?php echo loadClass('Helper')->getEnv('LOCAL_LISTEN_ADDR').loadClass('Helper')->getEnv('HOST_PORT_HTTPD_SSL');?>
												</td>
												<td>80<br/>443</td>
											</tr>
											<?php if ($avail_mysql): ?>
												<tr>
													<th>mysql</th>
													<td><?php echo loadClass('Helper')->getEnv('LOCAL_LISTEN_ADDR').loadClass('Helper')->getEnv('HOST_PORT_MYSQL');?></td>
													<td>3306</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_oracle): ?>
												<tr>
													<th>oracle</th>
													<td>1521</td>
													<td>1521</td>
												</tr>
											<?php endif; ?>
											
											<?php if ($avail_dns): ?>
												<tr>
													<th>bind</th>
													<td>
														<?php echo loadClass('Helper')->getEnv('LOCAL_LISTEN_ADDR').loadClass('Helper')->getEnv('HOST_PORT_BIND');?>/tcp<br/>
														<?php echo loadClass('Helper')->getEnv('LOCAL_LISTEN_ADDR').loadClass('Helper')->getEnv('HOST_PORT_BIND');?>/udp
														</td>
													<td>53/tcp<br/>53/udp</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_couchbase): ?>
												<tr>
												<th>couchbase</th>
												<td>8091</td>
												<td>8091</td>
											</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-hdd-o" aria-hidden="true"></i> Data mounts</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="container">
									<table class="table table-striped table-hover table-bordered table-sm font-small" style="word-break: break-word;">
										<thead class="thead-inverse">
											<tr>
												<th>Docker</th>
												<th>Host path</th>
												<th>Docker path</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>php</th>
													<td><?php echo loadClass('Helper')->getEnv('HOST_PATH_HTTPD_DATADIR'); ?></td>
												<td>/shared/httpd</td>
											</tr>
											<tr>
												<th>httpd</th>
													<td><?php echo loadClass('Helper')->getEnv('HOST_PATH_HTTPD_DATADIR'); ?></td>
												<td>/shared/httpd</td>
											</tr>
											<?php if ($avail_mysql): ?>
												<tr>
													<th>mysql</th>
													<td><?php echo loadClass('Helper')->getEnv('HOST_PATH_MYSQL_DATADIR').'/'.loadClass('Helper')->getEnv('MYSQL_SERVER'); ?></td>
													<td>/var/lib/mysql</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_oracle): ?>
												<tr>
													<th>oracle</th>
													<td>/data/oracle</td>
													<td>/u01/app/oracle</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_dns): ?>
												<tr>
													<th>bind</th>
													<td>-</td>
													<td>-</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_couchbase): ?>
													<tr>
													<th>couchbase</th>
													<td>./data/couchbase</td>
													<td>/opt/couchbase/var/lib/couchbase</td>
												</tr>
											<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-cogs" aria-hidden="true"></i> Config mounts</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="container">
									<table class="table table-striped table-hover table-bordered table-sm font-small">
										<thead class="thead-inverse">
											<tr>
												<th>Docker</th>
												<th>Host path</th>
												<th>Docker path</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>php (ini)</th>
												<td>./cfg/php-ini-<?php echo loadClass('Helper')->getEnv('PHP_SERVER'); ?></td>
												<td>/etc/php-custom.d</td>
											</tr>
											<tr>
												<th>php (fpm)</th>
												<td>./cfg/php-fpm-<?php echo loadClass('Helper')->getEnv('PHP_SERVER'); ?></td>
												<td>/etc/php-fpm-custom.d</td>
											</tr>
											<tr>
												<th>httpd</th>
												<td>./cfg/<?php echo loadClass('Helper')->getEnv('HTTPD_SERVER'); ?></td>
												<td>/etc/httpd-custom.d</td>
											</tr>
											<?php if ($avail_mysql): ?>
												<tr>
													<th>mysql</th>
													<td>./cfg/<?php echo loadClass('Helper')->getEnv('MYSQL_SERVER'); ?></td>
													<td>/etc/mysql/conf.d</td>
												</tr>
											<?php endif; ?>
											
											<?php if ($avail_dns): ?>
												<tr>
													<th>bind</th>
													<td>-</td>
													<td>-</td>
												</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 col-margin">
					<div class="dash-box">
						<div class="dash-box-head"><i class="fa fa-bar-chart" aria-hidden="true"></i> Log mounts</div>
						<div class="dash-box-body">
							<div class="row">
								<div class="container">
									<table class="table table-striped table-hover table-bordered table-sm font-small" style="word-break: break-word;">
										<thead class="thead-inverse">
											<tr>
												<th>Docker</th>
												<th>Host path</th>
												<th>Docker path</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>php</th>
												<td>./log/php-fpm-<?php echo loadClass('Helper')->getEnv('PHP_SERVER'); ?></td>
												<td>/var/log/php</td>
											</tr>
											<tr>
												<th>httpd</th>
												<td>./log/<?php echo loadClass('Helper')->getEnv('HTTPD_SERVER'); ?></td>
												<td>/var/log/<?php echo loadClass('Helper')->getEnv('HTTPD_SERVER'); ?></td>
											</tr>
											<?php if ($avail_mysql): ?>
												<tr>
													<th>mysql</th>
													<td>./log/<?php echo loadClass('Helper')->getEnv('MYSQL_SERVER'); ?></td>
													<td>/var/log/mysql</td>
												</tr>
											<?php endif; ?>
											
											<?php if ($avail_dns): ?>
												<tr>
													<th>bind</th>
													<td>-</td>
													<td>-</td>
												</tr>
											<?php endif; ?>
											<?php if ($avail_couchbase): ?>
											   <tr>
													<th>couchbase</th>
													<td>./log/couchbase</td>
													<td>/opt/couchbase/var/lib/couchbase/logs</td>
												</tr>
												<?php endif;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div><!-- /row -->


		</div><!-- /.container -->

		<?php echo loadClass('Html')->getFooter(); ?>
		<script>
		// self executing function here
		(function() {
			// your page initialization code here
			// the DOM will be available here

			/**
			 * Update installed tool versions.
			 * Ajax method is faster for loading the front page
			 * @param  string app Name of the tool
			 */
			function updateVersions(app) {
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					var elem = document.getElementById('app_'+app);

					if (this.readyState == 4 && this.status == 200) {
						json = JSON.parse(this.responseText);
						elem.innerHTML = json[app];
					}
				};
				xhttp.open('GET', '_ajax_callback.php?software='+app, true);
				xhttp.send();
			}
			updateVersions('composer');
			updateVersions('drupalc');
			updateVersions('drush');
			updateVersions('git');
			updateVersions('laravel');
			updateVersions('mds');
			updateVersions('node');
			updateVersions('npm');
			updateVersions('phalcon');
			updateVersions('symfony');
			updateVersions('wpcli');
		})();
		</script>
	</body>
</html>
