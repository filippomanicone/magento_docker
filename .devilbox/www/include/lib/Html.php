<?php
namespace devilbox;

class Html
{

	/**
	 * The Devilbox Navbar menu
	 * @var array
	 */
	private $_menu = array(
		array(
			array(
				'name' => 'Home',
				'path' => '/index.php'
			),
			array(
				'name' => 'Virtual Hosts',
				'path' => '/vhosts.php'
			),
			array(
				'name' => 'Emails',
				'path' => '/mail.php'
			)
		),
		array(
			'name' => 'Databases',
			'menu' => array(
				array(
					'name' => 'MySQL DB',
					'path' => '/db_mysql.php'
				),
				array(
					'name' => 'CouchBase',
					'path' => 'http://localhost:8091',
					'target' => '_blank'
				),
			    array(
			        'name' => 'Oracle',
			        'path' => 'http://localhost:8080/apex',
			        'target' => '_blank'
			    ),
			)
		),
		array(
			'name' => 'Info',
			'menu' => array(
				array(
					'name' => 'Httpd Info',
					'path' => '/info_httpd.php'
				),
				array(
					'name' => 'PHP Info',
					'path' => '/info_php.php'
				),
				array(
					'name' => 'MySQL Info',
					'path' => '/info_mysql.php'
				),
			)
		),
		array(
			'name' => 'Tools',
			'menu' => array(
				array(
					'name' => 'Adminer',
					'path' => '/vendor/adminer-4.6.2-en.php',
					'target' => '_blank'
				),
				array(
					'name' => 'phpMyAdmin',
					'path' => '__PHPMYADMIN__',
					'target' => '_blank'
				),
				array(
					'name' => 'Opcache GUI',
					'path' => '/opcache.php'
				)
			)
		)
	);



	/*********************************************************************************
	 *
	 * Statics
	 *
	 *********************************************************************************/

	/**
	 * $this Singleton instance
	 * @var Object|null
	 */
	protected static $_instance = null;

	/**
	 * Singleton Instance getter.
	 *
	 * @return object|null
	 */
	public static function getInstance()
	{
		if (self::$_instance === null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}



	/*********************************************************************************
	 *
	 * Select functions
	 *
	 *********************************************************************************/


	public function getHead($font_awesome = false)
	{
		$css_fa = ($font_awesome) ? '<link href="/vendor/font-awesome/font-awesome.min.css" rel="stylesheet">' : '';

		$html = <<<HTML
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

			<!-- Meta -->
			<meta name="description" content="Magento.">
			<meta name="author" content="cytopia">

			<!-- Favicons -->
			<link rel="manifest" href="/manifest.json">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
			<meta name="theme-color" content="#ffffff">

			<!-- CSS/JS -->
			<link href="/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
			{$css_fa}
			<link href="/assets/css/custom.css" rel="stylesheet">

			<title>The Magento</title>
HTML;
		return $html;
	}


	public function getNavbar()
	{
		$menu = $this->_buildMenu();
		$logout = '';
		if (loadClass('Helper')->isLoginProtected()) {
			$logout =	'<ul class="navbar-nav">'.
							'<li class="nav-item text-right"><a class="nav-link" href="/logout.php?id='.session_id().'">Log out</a></li>'.
						'</ul>';
		}

		$html = <<<HTML
			<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/index.php">
					<img src="/assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">magento
				</a>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">

						{$menu}


					</ul>
					{$logout}
				</div>

			</nav>
			<br/>
HTML;
		return $html;
	}



	public function getFooter()
	{
		$render_time = round((microtime(true) - $GLOBALS['TIME_START']), 2);
		$errors =  loadClass('Logger')->countErrors();

		$html = <<<HTML
			<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse footer">
				<div class="container justify-content-end">
					<ul class="nav navbar-nav">
						<li class="nav-item nav-link">Render time: ${render_time} sec</li>
						<li class="nav-item"><a class="nav-link" href="/debug.php"><code>Debug ({$errors})</code></a></li>
					</ul>
				</div>
			</nav>

			<script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
			<script src="/vendor/tether/tether.min.js"></script>
			<script src="/vendor/bootstrap/bootstrap.min.js"></script>
HTML;
		return $html;
	}



	public function getCirle($name)
	{
		switch ($name) {
			case 'dns':
				$class = 'bg-info';
				$version = loadClass('Dns')->getVersion();
				$available = loadClass('Dns')->isAvailable();
				$name = loadClass('Dns')->getName();
				break;
			case 'php':
				$class = 'bg-info';
				$version = loadClass('Php')->getVersion();
				$available = loadClass('Php')->isAvailable();
				$name = loadClass('Php')->getName();
				break;
			case 'httpd':
				$class = 'bg-info';
				$version = loadClass('Httpd')->getVersion();
				$available = loadClass('Httpd')->isAvailable();
				$name = loadClass('Httpd')->getName();
				break;
			case 'mysql':
				$class = 'bg-warning';
				$version = loadClass('Mysql')->getVersion();
				$available = loadClass('Mysql')->isAvailable();
				$name = loadClass('Mysql')->getName();
				break;
			case 'oracle':
			    $class = 'bg-warning';
			    $version = loadClass('Oracle')->getVersion();
			    $available = loadClass('Oracle')->isAvailable();
			    $name = loadClass('Oracle')->getName();
			    break;
			case 'couchbase':
			    $class = 'bg-warning';
			    $version = loadClass('MyCouchBase')->getVersion();
			    $available = loadClass('MyCouchBase')->isAvailable();
			    $name = loadClass('MyCouchBase')->getName();
			    break;
			default:
				$available = false;
				$version = '';
				break;
		}

		$style = 'color:black;';
		$version = '('.$version.')';
		if (!$available) {
			$class = '';
			$style = 'background-color:gray;';
			$version = '&nbsp;';
		}
		$circle = '<div class="circles">'.
					'<div>'.
						'<div class="'.$class.'" style="'.$style.'">'.
							'<div>'.
								'<div><br/><strong>'.$name.'</strong><br/><small style="color:#333333">'.$version.'</small></div>'.
							'</div>'.
						'</div>'.
					'</div>'.
				'</div>';
		return $circle;
	}



	/*********************************************************************************
	 *
	 * Private functions
	 *
	 *********************************************************************************/

	private function _buildMenu()
	{

		$path = $_SERVER['PHP_SELF'];
		$html = '';

		foreach ($this->_menu as $type => $elements) {

			// Menu
			if (!isset($elements['menu'])) {

				foreach ($elements as $el) {
					if ($path == $el['path']) {
						$class = 'active';
						$span = '<span class="sr-only">(current)</span>';
					} else {
						$class = '';
						$span = '';
					}

					$html .= '<li class="nav-item '.$class.'">';
					$html .= 	'<a class="nav-link" href="'.$el['path'].'">'.$el['name'].' '.$span.'</a>';
					$html .= '</li>';
				}

			// Submenu
			} else {
				$name = $elements['name'];
				$class = '';
				$id	= md5($name);


				// Make submenu active
				foreach ($elements['menu'] as $el) {
					if (strpos($path, $el['path']) !== false) {
						$class = 'active';
						break;
					}
				}

				$html .= '<li class="nav-item dropdown '.$class.'">';
				$html .=	'<a class="nav-link dropdown-toggle" href="#" id="'.$id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
				$html .=    	$name;
				$html .=	'</a>';
				$html .=	'<div class="dropdown-menu" aria-labelledby="'.$id.'">';

				foreach ($elements['menu'] as $el) {

					// Replace
					if ($el['path'] == '__PHPMYADMIN__') {
						$el['path'] = (strpos(loadClass('Php')->getVersion(), '5.4') !== false) ? '/vendor/phpmyadmin-4.0/index.php' : '/vendor/phpmyadmin-4.8/index.php';
					}

					$target = isset($el['target']) ? 'target="'.$el['target'].'"' : '';
					$html .= '<a class="dropdown-item" '.$target.' href="'.$el['path'].'">'.$el['name'].'</a>';
				}

				$html .=	'</div>';
				$html .= '</li>';
			}
		}

		return $html;
	}
}
