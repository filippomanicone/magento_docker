<?php
namespace devilbox;

/**
 * @requires devilbox::Logger
 */
class Oracle extends BaseClass implements BaseInterface
{
    
    /*********************************************************************************
     *
     * Variables
     *
     *********************************************************************************/
    
    /**
     * MySQL connection link
     * @var null
     */
    private $_link = null;
    
    
    
    /*********************************************************************************
     *
     * Constructor Overwrite
     *
     *********************************************************************************/
    
    public function __construct($hostname, $data = array())
    {
        parent::__construct($hostname, $data);
        
        $user = $data['user'];
        $pass = $data['pass'];
        
        
        // Silence errors and try to connect
        error_reporting(0);
        $link = @oci_connect($user, $pass, $hostname."/XE");
        error_reporting(-1);
        
        if (!$link) {
            $error = oci_error();
            $this->setConnectError('Failed to connect: ' .$error['message']);
            $this->setConnectErrno($error['message']);
            //loadClass('Logger')->error($this->_connect_error);
        } else {
            $this->_link = $link;
        }
    }
    
    public function __destruct()
    {
        if ($this->_link) {
            oci_close($this->_link);
        }
    }
    
    
    /*********************************************************************************
     *
     * Select Functions
     *
     *********************************************************************************/
    
    /**
     * Query Database
     *
     * @param  string   $query    MySQL Query
     * @param  function $callback Callback function
     * @return mixed[]
     */
    public function select($query, $callback = null)
    {}
    
    /**
     * Get all MySQL Databases.
     * @return mixed[] Array of databases
     */
    public function getDatabases()
    {}
    
    /**
     * Get Database size in Megabytes.
     *
     * @param  string $database Database name.
     * @return integer
     */
    public function getDBSize($database)
    {}
    
    /**
     * Get Number of Tables per Database
     *
     * @param  string $database Database name.
     * @return integer
     */
    public function getTableCount($database)
    {}
    
    
    /**
     * Read out MySQL Server configuration by variable
     *
     * @param  string|null $key Config key name
     * @return string|mixed[]
     */
    public function getConfig($key = null)
    {}
    
    
    
    /*********************************************************************************
     *
     * Interface required functions
     *
     *********************************************************************************/
    
    private $_can_connect = array();
    private $_can_connect_err = array();
    
    private $_name = null;
    private $_version = null;
    
    
    public function getName($default = 'Oracle')
    {
        return $default;
    }
    
    public function getVersion()
    {
        $this->_version=oci_server_version($this->_link );
        return $this->_version;
    }
    
    public function canConnect(&$err, $hostname, $data = array())
    {
        $err = false;
        
        // Return if already cached
        if (isset($this->_can_connect[$hostname])) {
            // Assume error for unset error message
            $err = isset($this->_can_connect_err[$hostname]) ? $this->_can_connect_err[$hostname] : true;
            return $this->_can_connect[$hostname];
        }
        
        // Silence errors and try to connect
        error_reporting(0);
        $link = @oci_connect( $data['user'], $data['pass'], $hostname."/XE");
        error_reporting(-1);
        
        if (!$link) {
            $error = oci_error();
            $err = 'Failed to connect: ' .$error['message'];
            $this->_can_connect[$hostname] = false;
        } else {
            $this->_can_connect[$hostname] = true;
        }
        
        if ($link) {
            oci_close($link);
        }
        
        $this->_can_connect_err[$hostname] = $err;
        return $this->_can_connect[$hostname];
    }
    

}
