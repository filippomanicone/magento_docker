<?php
namespace devilbox;
/**
 * @requires devilbox::Logger
 */
class MyCouchBase extends BaseClass implements BaseInterface
{
    /*********************************************************************************
     *
     * Private Variables
     *
     *********************************************************************************/
    /**
     * MongoDB manager instance
     * @var object|null
     */
    private $_couchbase = null;
    /*********************************************************************************
     *
     * Constructor Overwrite
     *
     *********************************************************************************/
    /**
     * Use singleton getInstance() instead.
     *
     * @param string $user Username
     * @param string $pass Password
     * @param string $host Host
     */
    public function __construct($hostname, $data = array())
    {
        $user = $data['user'];
        $pass = $data['pass'];
        parent::__construct($hostname, $data);
        // Faster check if mongo is not loaded
        $cluster = new \Couchbase($hostname.":8091", "", "", "default", true);
        if ($cluster == null)
        {
            $this->setConnectError('Failed to connect CouchBase');
            $this->setConnectErrno('Failed to connect CouchBase');
        }else{
            $this->_link = $cluster;
            
        }
        
       
    }
    /*********************************************************************************
     *
     * Select functions
     *
     *********************************************************************************/
    /**
     * Execute MongoDB command and return iteratable
     * @param  array      $command Command
     * @return iteratable
     */
    private function command($command)
    {
    }
    /**
     * Get all MongoDB Databases.
     * @return mixed[] Array of databases
     */
    public function getDatabases()
    {}
    public function getInfo()
    {}
    /*********************************************************************************
     *
     * Interface required functions
     *
     *********************************************************************************/
    private $_can_connect = array();
    private $_can_connect_err = array();
    private $_name = null;
    private $_version = null;
    public function canConnect(&$err, $hostname, $data = array())
    {
        $err = false;
        // Return if already cached
        if (isset($this->_can_connect[$hostname])) {
            // Assume error for unset error message
            $err = isset($this->_can_connect_err[$hostname]) ? $this->_can_connect_err[$hostname] : true;
            return $this->_can_connect[$hostname];
        }
        $cluster = new \Couchbase($hostname.":8091", "", "", "default", true);
        

        if (!$cluster) {
            $err = 'Failed to connect to CouchBase host on '.$hostname.' (No host info available)';
            $this->_can_connect[$hostname] = false;
        } else{
            $err = 'connect to CouchBase host on '.$hostname;
            $this->_can_connect[$hostname] = true;
        }
        
        $this->_can_connect_err[$hostname] = $err;
        return $this->_can_connect[$hostname];
    }
    public function getName($default = 'CouchBase')
    {
        return $default;
    }
    public function getVersion()
    {

        return null;
    }
}